import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patient-registration',
  templateUrl: './patient-registration.component.html',
  styleUrls: ['./patient-registration.component.css']
})
export class PatientRegistrationComponent implements OnInit {
  demographicsForm: FormGroup;
  loading = false;
  pageTitle = "Patient Registration";

  filteredCountries = [];
  filteredCities = [];
  filteredProvinces = [];

  patient: any;
  patientClone: any;

  editMode: boolean = false;
  message: string;
  saved: boolean = false;

  isRetired;
  showVisitTokenDialog: boolean = false;

  generatedMrn: string;
  generatedToken: number;
  constructor(
    private fb: FormBuilder,
    // private datePipe: DatePipe,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {
    this.generateForms();
  }
  ngOnInit() {
    // this.activatedRoute.data.subscribe(data => {
    //   this.patient = data["patient"];
    //   if (this.patient.uuid !== null && this.patient.uuid !== undefined) {
    //     this.editMode = true;
    //     this.saved = true;
    //     this.populateForm();
    //     this.patientClone = JSON.parse(JSON.stringify(this.patient));
    //   }
    // });

    
    this.demographicsForm.valueChanges.subscribe(res => {
      // this.patient.person = this.demographicsForm.value;
      if (this.patientClone && JSON.stringify(this.patientClone) !== JSON.stringify(this.patient)) {
        this.saved = false;
      }

      if (res.age > 0) {
        let dob = res.birthdate;
        let date = new Date();
        date.setFullYear(date.getFullYear() - (res.age));
        date.setHours(0);
        date.setSeconds(0);
        date.setMinutes(0);
        date.setMilliseconds(0.0);
        if (dob === "") {
          this.demographicsForm.controls.birthdate.patchValue(date);
        } else {
          dob.setHours(0);
          dob.setSeconds(0);
          dob.setMinutes(0);
          dob.setMilliseconds(0.0);
          if (date.getTime() !== dob.getTime()) {
            this.demographicsForm.controls.birthdate.patchValue(date);
          }
        }
      }
    })
  }

  //-------Form Handling Methods----------

  searchCountry(query, countries: any[]): any[] {
    // in a real application, make a request to a remote url with the query and return filtered results,
    // for demo we filter at client side
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }

  searchCities(query, cities: any[]): any[] {
    // in a real application, make a request to a remote url with the query and return filtered results,
    // for demo we filter at client side
    const filtered: any[] = [];
    for (let i = 0; i < cities.length; i++) {
      const city = cities[i];
      if (city.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(city);
      }
    }
    return filtered;
  }

  searchProvinces(query, provinces: any[]): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < provinces.length; i++) {
      const province = provinces[i];
      if (province.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(province);
      }
    }
    return filtered;
  }
  buildPhoneNoAttribute(): FormGroup {
    let phonePattern = "^((\\+92)|(0092))-{0,1}\\d{3}-{0,1}\\d{7}$|^\\d{11}$|^\\d{4}-\\d{7}$";
    return this.fb.group({
      value: ['', [ /*Validators.pattern(phonePattern)*/]]
    });
  }

  buildNames(): FormGroup {
    let nameRegex: string = "[a-z A-Z]+";
    return this.fb.group({
      givenName: ['', [Validators.required, Validators.minLength(3), Validators.pattern(nameRegex)]],
      middleName: ['', [Validators.minLength(3), Validators.pattern(nameRegex)]],
      familyName: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(nameRegex)]]
    });
  }

  buildAddress(): FormGroup {
    return this.fb.group({
      address1: [''],
      address2: ['', /*Validators.pattern(nameRegex)*/],
      country: [''],
      stateProvince: [''],
      cityVillage: [''],
      postalCode: ['']
    });
  }

  generateForms() {
    this.demographicsForm = this.fb.group(
      {
        names: this.fb.array([this.buildNames()]),
        gender: ['', Validators.required],
        birthdate: ['', Validators.required],
        age: '',
        attributes: this.fb.array([this.buildPhoneNoAttribute()]),
        addresses: this.fb.array([this.buildAddress()])
      });
  }

  populateForm() {
    this.generatedMrn = this.patient.identifiers[0].identifier;
    this.demographicsForm.patchValue({
      names: this.patient.person.names,
      gender: this.patient.person.gender,
      attributes: this.patient.person.attributes,
      birthdate: new Date(this.patient.person.birthdate),
      addresses: this.patient.person.addresses
    }, { emitEvent: false, onlySelf: true });
    //if (this.patient.person.attributes[6].value)
    // this.isRetired = this.patient.person.attributes[6].value;
    if (this.patient.person.addresses && this.patient.person.addresses.length > 0) {
      this.demographicsForm.patchValue({
        addresses: [
          {
            "country": { "name": this.patient.person.addresses[0].country },
            "cityVillage": { "name": this.patient.person.addresses[0].cityVillage },
            "stateProvince": { "name": this.patient.person.addresses[0].stateProvince }
          }
        ]
      });
    }
  }
  //-------Form Handling Methods----------
  //------------Logic Methods------------START


  onSaveComplete(message?: string): void {
    this.message = message;
    if (message.toLocaleLowerCase().includes('error')) {
      // this.notifyError("Error occured");
      return;
    }
    let uuid: string = this.patient.uuid;
    if (!this.editMode) {
      // this.notifySuccess('New Registration', `Patient '${this.patient.display}' Added Successfully`);
    }
    else {
      // this.notifySuccess('Patient Update', `Patient '${this.patient.display}' Edited Successfully`);
      this.router.navigate(['albasr/clinic/patients', this.patient.uuid]);
    }

    this.saved = true;
    this.patientClone = JSON.parse(JSON.stringify(this.patient));
    //this.router.navigate(['/patients', uuid]);
  }
  saveBtnClick() {
    this.loading = true;

    // this.demographicsForm.value.birthdate = this.datePipe.transform(this.demographicsForm.value.birthdate, "yyyy-MM-dd");
    this.demographicsForm.value.addresses[0].stateProvince = this.demographicsForm.value.addresses[0].stateProvince.name;
    this.demographicsForm.value.addresses[0].cityVillage = this.demographicsForm.value.addresses[0].cityVillage.name;
    this.demographicsForm.value.addresses[0].country = this.demographicsForm.value.addresses[0].country.name;


    delete this.patient.person.age;
    //this.saved = true;
    if (!this.editMode) {
      // this.createPatient();
      this.demographicsForm.disable();
    }
    else {
      // this.updatePatient();
    }
  }
  //---------Events-----------END

  // printDocument() {
  //   // this.patientVisitService.printConsultationForm(this.patient, {}).subscribe(res => {
  //     res.open();
  //   });
  // }
  // startNewVisit() {
  //   this.loading = true;
  //   let visitPayload = {
  //     patient: this.patient.uuid,
  //     visitType: this.appConstantsService.visitTypes.opdVisitTypeUUID,
  //     location: this.appConstantsService.defaultLocationUUID
  //   }
  //   this.visitService.createVisit(visitPayload).subscribe(visit => {
  //     // this.patientVisitService.printConsultationForm(this.patient, visit).subscribe(res => {
  //     this.localStorageCounterService.incrementCounter();

  //     // res.open();
  //     // });
  //     this.router.navigateByUrl("albasr/clinic/patients/" + this.patient.uuid + "/visits/" + visit.uuid);
  //     this.loading = false;
  //   })
  // }

  // notifyError(summary: string, detail: string = "") {
  //   this.messageService.add({ severity: 'error', summary: summary, detail: detail });
  // }
  // notifySuccess(summary: string, detail: string = "") {
  //   this.messageService.add({ severity: 'success', summary: summary, detail: detail });
  // }

  onCloseDialogEvent($event) {
    this.showVisitTokenDialog = false;
  }

}

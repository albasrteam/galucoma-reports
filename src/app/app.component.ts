import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  image: any;
  ngOnInit() {

  }
  title = 'app';
  cardLoop = ["Disk", "Cup", "Rim", "Inferier", "Superier", "Nasal", "Temporal"];
  newImage: any;
  cars: any[] = [
    { vin: 'Test', year: 'Test', brand: 'Test', color: 'Test' }
  ];
  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
    }
    let base64 = myReader.readAsDataURL(file);
    setTimeout(() => {
      console.log("Base64 Image: ", myReader.result);
      this.newImage = myReader.result;
    }, 500);
  }

  print(): void {
    // let printContents, popupWin;
    // printContents = document.getElementById('print-section').innerHTML;
    // (window as any).print();
    var printContents = document.getElementById('print-section').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    (window as any).print();
    document.body.innerHTML = originalContents;
  }
}

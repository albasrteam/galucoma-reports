import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { PatientRegistrationComponent } from './patient-registration/patient-registration.component';
import { ReportsComponent } from './reports/reports.component';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { CalendarModule } from 'primeng/calendar';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { FieldsetModule } from 'primeng/fieldset';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SpinnerModule } from 'primeng/spinner';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/components/common/messageservice';
import { InputTextareaModule } from 'primeng/inputtextarea';

const appRoutes: Routes = [
  { path: '', component: PatientRegistrationComponent },
  { path: 'registration', component: PatientRegistrationComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'upload-files', component: FileUploadComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    PatientRegistrationComponent,
    ReportsComponent,
    FileUploadComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    PanelModule,
    CardModule,
    CalendarModule,
    FieldsetModule,
    RadioButtonModule,
    SpinnerModule,
    ProgressSpinnerModule,
    AutoCompleteModule,
    InputMaskModule,
    InputTextModule,
    FileUploadModule,
    ToastModule,
    InputTextareaModule,
    ReactiveFormsModule


  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
